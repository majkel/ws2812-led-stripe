/*
 * pixel_strand.c
 *
 * Created: 08.03.2017 20:21:45
 * Author : Michał Butkiewicz
 */ 

 #ifndef F_CPU
 #define F_CPU 16000000UL // 16 MHz clock speed
 #endif

#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "light_ws2812.h"

/* #define LED_PIN PINB5 */

#define MAXPIX 150

struct cRGB led[MAXPIX];

enum colors {OFF, RED, GREEN, BLUE, PINK, YELLOW, TURKIS, LIGHTRED, LIGHTGREEN, LIGHTBLUE, WHITE};
#define COLOR_LEN 11



void led_on() {
	PORTB |= (1<<PINB5);
}


void led_off() {
	PORTB &= ~(1<<PINB5);
}


void delay_ms(int ms) {
	led_on();
	for (int i=0; i<ms; i++) _delay_ms(1);
	led_off();
}


void update_strip() {
	ws2812_setleds(led, 3*MAXPIX);
}


void prep_pixel(uint8_t position, uint8_t r, uint8_t g, uint8_t b) {
	led[position].r = r;
	led[position].g = g;
	led[position].b = b;
}


void prep_pixel_fixedcolor(uint8_t position, uint8_t color_arr[3]) {
	led[position].r = color_arr[0];
	led[position].g = color_arr[1];
	led[position].b = color_arr[2];
}


void strip_onecolor(uint8_t color_arr[3]) {
	for (uint8_t i=0; i<MAXPIX; i++)
		prep_pixel(i, color_arr[0], color_arr[1], color_arr[2]);

	update_strip();
}


void strip_customcolor(uint8_t r, uint8_t g, uint8_t b) {
	for (uint8_t i=0; i<MAXPIX; i++)
		prep_pixel(i, r, g, b);

	update_strip();
}


void clear_strip() {
	strip_customcolor(0, 0, 0);
	update_strip();
}


void update_pixel(uint8_t position, uint8_t r, uint8_t g, uint8_t b) {
	prep_pixel(position, r, g, b);
	update_strip();
}


void update_pixel_fixedcolor (int position, uint8_t color_arr[3]) {
	prep_pixel(position, color_arr[0],color_arr[1], color_arr[2]);
	update_strip();
}


void strip_colorswipe(int delay, uint8_t color_arr[3]) {
	for (uint8_t i=0; i<MAXPIX; i++) {
		update_pixel(i, color_arr[0], color_arr[1], color_arr[2]);
		delay_ms(delay);
	}
}


void strip_nazi(uint8_t color_A[3], uint8_t color_B[3], uint8_t color_C[3]) {
	int i, cnt=0;

	for (i=140; i>=103; i--) {
		update_pixel_fixedcolor(i, color_A);
		update_pixel_fixedcolor(i-75, color_A);
		_delay_ms(10);
	}

	_delay_ms(1500);

	for (i=102; i>=66; i--) {
		update_pixel_fixedcolor(i, color_B);
		if (i-75 >= 0) update_pixel_fixedcolor(i-75, color_B);
		else { 
			update_pixel_fixedcolor(MAXPIX-1-cnt, color_B);
			cnt++;
		}
		_delay_ms(10);
	}

	_delay_ms(1500);

	cnt = 0;
	for (i=140; i>=66; i--) {
		update_pixel_fixedcolor(i, color_C);
		if (i-75 >= 0) update_pixel_fixedcolor(i-75, color_C);
		else {
			update_pixel_fixedcolor(MAXPIX-1-cnt,color_C);
			cnt++;
		}
		_delay_ms(10);
	}
}


void strip_fadetocolor(int delay, uint8_t color_arr[3]) {
	int cnt = 0;
	
	for (uint8_t i=0; i<256; i++) {
		for(uint8_t n=0; n<MAXPIX; n++) {
			if (led[n].r < color_arr[0]) led[n].r++;
			else if (led[n].r > color_arr[0]) led[n].r--;
			else cnt++;
			
			if (led[n].g < color_arr[1]) led[n].g++;
			else if (led[n].g > color_arr[1]) led[n].g--;
			else cnt++;
			
			if (led[n].b < color_arr[2]) led[n].b++;
			else if (led[n].b > color_arr[2]) led[n].b--;
			else cnt++;
		}

		update_strip();

		if (cnt == 3*MAXPIX) break;
		cnt = 0;

		delay_ms(delay);
	}
}


void _chase(int position, uint8_t color_A[3], uint8_t color_B[3]) {
	prep_pixel_fixedcolor(position, color_B);

	if (position > 0) {
		if (position < MAXPIX-2) prep_pixel_fixedcolor(position+1, color_B);
		prep_pixel_fixedcolor(position-1, color_A);
	}
}


void strip_chase(int delay, uint8_t repeats, uint8_t color_A[3], uint8_t color_B[3]) {	
	for (uint8_t r=0; r<repeats; r++) {
	
		for (uint8_t i=0; i<MAXPIX+101; i++) {
			_chase(i, color_A, color_B);

			for (uint8_t n=20; n<=100; n+=20) if (i >= !(i % n) && i > n-1) _chase(i-n, color_A, color_B);

			update_strip();

			if (i == MAXPIX-2 && r == repeats-1) break;
			_delay_ms(10);
		}
	}
} 




int main(void)
{
	DDRB |= (1<<PINB5);

	int wait;

	uint8_t color[COLOR_LEN][3] = {
		{0, 0, 0},       // OFF
		{255, 0, 0},     // RED
		{0, 255, 0},     // GREEN
		{0, 0, 255},     // BLUE
		{255, 0, 100},   // PINK
		{100, 255, 0},   // YELLOW
		{0, 100, 255},   // TURKIS
		{200, 50, 100},  // LIGHTRED
		{100, 200, 50},  // LIGHTGREEN
		{50, 100, 200},  // LIGHBLUE
		{255, 255, 255}  // WHITE
	};


	clear_strip();

	// --- test area ---



	// --- test area ---

	strip_fadetocolor(10, color[TURKIS]);

	_delay_ms(5000);

	strip_fadetocolor(10, color[OFF]);

	while (1) {

		wait = 70;

		strip_colorswipe(wait, color[RED]);
		strip_colorswipe(wait, color[GREEN]);
		strip_colorswipe(wait, color[BLUE]);
		strip_colorswipe(wait, color[PINK]);
		strip_colorswipe(wait, color[YELLOW]);
		strip_colorswipe(wait, color[TURKIS]);
		
		_delay_ms(5000);

		strip_nazi(color[YELLOW], color[PINK], color[BLUE]);
		_delay_ms(1500);
		strip_nazi(color[WHITE], color[RED], color[GREEN]);
		_delay_ms(1500);
		strip_nazi(color[LIGHTBLUE], color[PINK], color[RED]);
		_delay_ms(1500);

		for (uint8_t i=2; i<COLOR_LEN; i++) strip_fadetocolor(5, color[i]);
		
		_delay_ms(5000);

		strip_chase(wait, 5, color[WHITE], color[RED]);
		strip_fadetocolor(wait, color[TURKIS]);
		strip_chase(wait, 5, color[TURKIS], color[PINK]);
		strip_fadetocolor(5, color[YELLOW]);
		strip_chase(wait, 5, color[YELLOW], color[BLUE]);
		strip_fadetocolor(5, color[RED]);
		strip_chase(wait, 5, color[RED], color[WHITE]);
		strip_fadetocolor(5, color[WHITE]);

		_delay_ms(15000);
		
	}
}

